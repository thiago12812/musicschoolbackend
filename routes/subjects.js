const router = require("express").Router();
let Subject = require('../models/subject.model');

router.route('/').get((req, res) => {
	Subject.find()
		.then(users => res.json(users))
		.catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) =>{ 
	const subjectName = req.body.subjectName;
	const teacher = req.body.teacher;
	const description = req.body.description;

	const newSubject = new Subject ({ subjectName, teacher, description });

	newSubject.save()
		.then(() => res.json('subject added'))
		.catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;