const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema ({
	name: { type: String, required: true },
	username: { type: String, required: true, trim: true, minlenght: 3 },
	email: { type: String, required: true },
	password: { type: String, required: true, minlenght: 5 },
	role: { type: String, required: true }
}, {
	timestamps: true,
});

const User = mongoose.model('User', userSchema);

module.exports = User;