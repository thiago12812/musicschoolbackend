const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const subjectSchema = new Schema ({
	subjectName: { type: String, required: true },
	teacher: { type: String, required: true, minlenght: 3},
	description: { type: String  }
}, {
	timestamps: true,
});

const Subject = mongoose.model('Subject', subjectSchema);

module.exports = Subject;